#/bin/bash

# Cria a rede
docker network create net-guaca

# MariaDB: Builda imagem personalizada e inicia container
docker build -t mariadb_guaca https://gitlab.com/Corolario/proj2/raw/master/MariaDB-Dockerfile
docker run --name some-mysql --network net-guaca -d mariadb_guaca

# guacacd: Inicia container
docker run --name some-guacd --network net-guaca -d guacamole/guacd

# guacamole: Inicia container
docker run --name some-guacamole -e GUACD_HOSTNAME=some-guacd -e MYSQL_HOSTNAME=some-mysql \
    -e MYSQL_USER=guaca_usuario -e MYSQL_PASSWORD=guaca_senha -e MYSQL_DATABASE=guaca_db \
    --network net-guaca -d -p 8080:8080 guacamole/guacamole

# Cria certificado
mkdir /certguaca
cd /certguaca
wget https://gitlab.com/Corolario/proj2/raw/master/obtem-cert.sh -O /certguaca/obtem-cert.sh
docker run --rm -t -v /certguaca/:/certguaca/ --entrypoint bash debian /certguaca/obtem-cert.sh
# rm /certguaca/obtem-cert.sh

# Nginx: Builda imagem de reverse-proxy e inicia container
wget https://gitlab.com/Corolario/proj2/raw/master/guaca.conf
wget https://gitlab.com/Corolario/proj2/raw/master/Nginx-Dockerfile
docker build -t nginx_guaca -f Nginx-Dockerfile .
docker run --name some-nginx --network net-guaca -d -p 443:443 nginx_guaca
